"""Scoreboard class"""

from turtle import Turtle


class Scoreboard(Turtle):
    """Scoreboard class"""

    def __init__(self):
        """Init scoreboard"""
        super().__init__()
        self.score = -1

        self.hideturtle()
        self.color("white")
        self.penup()
        self.goto(0, 275)
        self.increase()

    def increase(self):
        """Increase the score and print it"""
        self.score += 1
        score_message = "Score: " + str(self.score)
        self.clear()
        self.write(score_message, False, "center", ("Arial", 14, "normal"))

    def game_over(self):
        """Print GAME OVER"""
        self.goto(0, 0)
        self.write("GAME OVER", False, "center", ("Arial", 14, "normal"))
